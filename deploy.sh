#!/bin/bash

if ! command -v git &> /dev/null
then
    echo "Git is required"
    exit
fi

git push heroku master