#!/bin/bash

function finalize () {
    cp deployapp-firsttime.sh $1/
    cp deploy.sh $1/
    cp checkappstatus.sh $1/
    rm -fr $1/.git
    rm -fr $1/.github
    rm -f $1/.gitignore
}


if [ $# -lt 2 ]
then
    echo "Usage: $0 <app_type> <app_name>"
    exit 0
fi

if ! command -v heroku &> /dev/null
then
    echo "Heroku-CLI is required"
    exit
fi

if [ $1 == "nodejs" ]
then
    git clone https://github.com/heroku/node-js-getting-started.git $2
    finalize $2
    exit
fi

if [ $1 == "python" ]
then
    git clone https://github.com/heroku/python-getting-started.git $2
    finalize $2
    exit
fi

if [ $1 == "dotnet" ]
then
    if ! command -v dotnet &> /dev/null
    then
        echo "DotNet CLI required"
        exit
    fi
        if [ -z "$3" ]
        then
            dotnet new webapp -o $2
        else
            dotnet new $3 -o $2
        fi
    finalize $2
    exit
fi

# NOT YET IMPLEMENTED
# java
# php
# ruby
# go
# scala
# closure 