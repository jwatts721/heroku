@echo off

@REM Parameter check
if [%1]==[] goto usage

@REM Required software check
@WHERE git.exe > nul 2>nul
if %ERRORLEVEL% neq 0 goto required
@WHERE heroku.cmd > nul 2>nul
if %ERRORLEVEL% neq 0 goto required

if exist Program.cs goto dotnet:
heroku create %1 && git init && git add --all && git commit -m "Deployment" && heroku git:remote -a %1 && git push heroku master
goto exit

:dotnet
heroku create %1 --buildpack https://github.com/jincod/dotnetcore-buildpack && git init && git add --all && git commit -m "Deployment" && heroku git:remote -a %1 && git push heroku master
goto exit

:usage
@echo Usage: %0 ^<heroku_app_name^>
exit /B 1

:required
@echo Git and Heroku-CLI are required

:exit
exit /B 1