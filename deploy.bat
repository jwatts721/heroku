@echo off

@WHERE git.exe > nul 2>nul
if %ERRORLEVEL% neq 0 goto required

git push heroku master

:required
@echo GIT is required

:exit
exit /B 1