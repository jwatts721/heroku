@echo off

@REM Parameter check
if [%2]==[] goto usage

@REM Required software check
@WHERE heroku.cmd > nul 2>nul
if %ERRORLEVEL% neq 0 goto required

if "%1"=="nodejs" goto nodejs
if "%1"=="python" goto python
if "%1"=="dotnet" goto dotnet
if "%1"=="java" goto java
if "%1"=="php" goto php
if "%1"=="ruby" goto ruby
if "%1"=="go" goto go
if "%1"=="scala" goto scala
if "%1"=="closure" goto closure
echo No option available for %1
goto exit

:nodejs
git clone https://github.com/heroku/node-js-getting-started.git %2
goto finalize

:python
git clone https://github.com/heroku/python-getting-started.git %2
goto finalize

:dotnet
@WHERE dotnet.exe > nul 2>nul
if %ERRORLEVEL% neq 0 goto required_dotnet
if Not "%3"=="" goto dotnet_with_type
dotnet new webapp -o %2
goto finalize

:dotnet_with_type
dotnet new %3 -o %2
goto finalize

:java
echo "Not yet implemented"
goto exit

:php
echo "Not yet implemented"
goto exit

:ruby
echo "Not yet implemented"
goto exit

:go
echo "Not yet implemented"
goto exit

:scala
echo "Not yet implemented"
goto exit

:closure
echo "Not yet implemented"
goto exit

:usage
@echo Usage: %0 ^<app_type^> ^<app_name^>
@echo App Type Options: nodejs, python, dotnet, java, php, ruby, go, scala, closure
@echo 
@echo For dotnet apps, you can specify the app template in the third parameter. If empty, defaults to webapp
goto exit

:required
@echo Heroku CLI is required
goto exit

:required_dotnet
@echo DotNet CLI is required
goto exit

:error_dotnet_template
@echo No templates matched the input template name. Use dotnet new --list for short name options.
goto exit

:finalize
copy deployapp-firsttime.bat %2\
copy deploy.bat %2\
copy checkappstatus.bat %2\
echo Y | rmdir /s %2\.git
echo Y | rmdir /s %2\.github
echo Y | del %2\.gitignore
goto exit

:exit
exit /B 1