@echo off

@WHERE heroku.cmd > nul 2>nul
if %ERRORLEVEL% neq 0 goto required

heroku ps:scale web=1 && heroku open

exit /B 1

:required
@echo Heroku-CLI is required