#!/bin/bash

if ! command -v heroku &> /dev/null
then
    echo "Heroku-CLI is required"
    exit
fi

heroku ps:scale web=1 && heroku open