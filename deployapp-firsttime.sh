#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: $0 <heroku_app_name>"
    exit 0
fi

if ! command -v git &> /dev/null || ! command -v heroku &> /dev/null
then
    echo "Git and Heroku-CLI are required"
    exit
fi

if [ -e Program.cs ]
then
    heroku create $1 --buildpack https://github.com/jincod/dotnetcore-buildpack && git init && git add --all && git commit -m "Deployment" && heroku git:remote -a $1 && git push heroku master
    exit
fi

heroku create $1 && git init && git add --all && git commit -m "Deployment" && heroku git:remote -a $1 && git push heroku master